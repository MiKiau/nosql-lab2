package com.nosql.db;

import com.nosql.db.features.example.Account;
import com.nosql.db.features.example.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ExampleTests {
  @Autowired private AccountRepository repository;

  @Test
  void successful_findByName() {
    Account expected = getAccount();

    repository.save(expected);
    Account actual = repository.findByFirstName(expected.getFirstName());

    Assertions.assertEquals(expected.getFirstName(), actual.getFirstName());
    Assertions.assertEquals(expected.getLastName(), actual.getLastName());
  }

  private Account getAccount() {
    return new Account(1L, "Vardenis", "Pavardenis");
  }
}
