package com.nosql.db.features.example;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(value = "account")
public class Account {
  @Id private Long id;
  private String firstName;
  private String lastName;
}
