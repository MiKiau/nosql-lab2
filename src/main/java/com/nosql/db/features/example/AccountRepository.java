package com.nosql.db.features.example;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends MongoRepository<Account, String> {
  @Query("{firstName:'?0'}")
  Account findByFirstName(String firstName);
}
