package com.nosql.db.features.film;

import lombok.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Category {
    private Integer id;
    private String name;
}
