package com.nosql.db.features.film;

import com.nosql.db.features.actor.Actor;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(value = "film")
public class Film {
    @Id private Long id;
    private String title;
    private LocalDate releaseDate;
    private String description;
    private Integer length;
    private Double rating;
    private List<Actor> actor = new ArrayList<>();
    private List<Category> category = new ArrayList<>();
}
