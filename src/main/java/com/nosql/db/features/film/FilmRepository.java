package com.nosql.db.features.film;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface FilmRepository extends MongoRepository<Film, Double> {
    @Query("{rating:'?0'}")
    Film findByRating(Double rating);
}
