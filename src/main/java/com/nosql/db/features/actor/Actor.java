package com.nosql.db.features.actor;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(value = "actor")
public class Actor {
    @Id private Integer id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
}
