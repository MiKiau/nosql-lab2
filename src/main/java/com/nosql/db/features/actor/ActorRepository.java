package com.nosql.db.features.actor;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface ActorRepository extends MongoRepository<Actor, String> {
    @Query("{lastName:'?0'}")
    Actor findByLastName(String lastName);
}
