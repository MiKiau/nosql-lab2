package com.nosql.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class NosqlLab2Application {
  public static void main(String[] args) {
    SpringApplication.run(NosqlLab2Application.class, args);
  }
}
