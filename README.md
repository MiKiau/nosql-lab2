# Nereliacinių duomenų bazių 2 laboratorinis darbas

## Užduotis

Sumodeliuokite duomenų bazę tinkamą dokumentų modeliui (modelį pateikite grafiniu formatu). Parašykite programą, kuri atlieka operacijas pagal reikalavimus.

Dalykinėje srityje turi būti bent 3 esybės. Sumodeliuokite bent dvi atskirose kolekcijose, bent dvi esybės turinčios kompozicijos sąryšį turi būti modeliuojamos tame pačiame dokumente (angl. embedded).

## Dokumentų modelis

Vaizduojamame UML modelyje turime tris esybes (actor, film, category). Film ir category esybės sukurtos toje pačioje kolekcijoje. Actor bei film esybės susietos agregacijos sąryšiu, o category bei film kompozicijos sąryšiu. Kompozicijos sąryšis parinktas dėl to, jog category esybė neturi savo dokumento bei priklauso film dokumentui.

![MongoDB dokumentų modelis](noSQL2.png "Grafinis dokumentų modelis")
