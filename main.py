import pymongo
from bson.code import Code
client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["filmsdb"] #database filmsdb
films = db["films"] #films collection
directors = db["directors"] #directors collection
films_list = [
  {"_id": 1, "title": "Movie1", "description": "Movie about great things1", "rating": 7.8, "actor": [
      {
        "_id": 2,
        "firstName": "John",
        "lastName": "Black"
      },
      {
        "_id": 5,
        "firstName": "Layla",
        "lastName": "White"
      }
    ],
    "category": "kategorija1", "directorID": 3},
  {"_id": 2, "title": "Movie2", "description": "Movie about great things1", "rating": 4.6, "actor": [
    {
        "_id": 6,
        "firstName": "Jenny",
        "lastName": "Fromtheblock"
      },
      {
        "_id": 5,
        "firstName": "Layla",
        "lastName": "White"
      }
    ],
    "category": "kategorija2", "directorID": 7},
  {"_id": 3, "title": "Movie3", "description": "Movie about great things1", "rating": 7.6, "actor": [
    {
        "_id": 4,
        "firstName": "Kristaps",
        "lastName": "Strielniks"
      },
      {
        "id": 10,
        "firstName": "Samara",
        "lastName": "Brown"
      }
  ],
    "category": "kategorija1", "directorID": 3},
  {"_id": 4, "title": "Movie4", "description": "Movie about great things1", "rating": 9.6, "actor": [
    {
      "_id": 10,
      "firstName": "Samara",
      "lastName": "Brown"
    },
      {
        "_id": 5,
        "firstName": "Layla",
        "lastName": "White"
      }
  ],
    "category": "kategorija6", "directorID": 5},
  {"_id": 5, "title": "Movie5", "description": "Movie about great things1", "rating": 7.1, "actor": [
    {
        "_id": 1,
        "firstName": "Liudas",
        "lastName": "Vasaris"
      },
      {
        "_id": 5,
        "firstName": "Layla",
        "lastName": "White"
      }
  ],
    "category": "kategorija1", "directorID": 3},
  {"_id": 6, "title": "Movie6", "description": "Movie about great things1", "rating": 2.6, "actor": [
    {
        "_id": 2,
        "firstName": "John",
        "lastName": "Black"
      },
      {
        "_id": 11,
        "firstName": "LeBron",
        "lastName": "James"
      }
  ],
    "category": "kategorija7", "directorID": 7},
  {"_id": 7, "title": "Movie7", "description": "Movie about great things1", "rating": 3.3, "actor": [
    {
        "_id": 2,
        "firstName": "John",
        "lastName": "Black"
      },
      {
        "_id": 15,
        "firstName": "John",
        "lastName": "Newman"
      }
  ],
    "category": "kategorija7", "directorID": 5},
]
directors_list = [
  {"_id": 3, "name": "Jorn Kirkman", "age": 48},
  {"_id": 5, "name": "Smelly Cat", "age": 35},
  {"_id": 7, "name": "John Smith", "age": 21}
]
db.directors.drop() #dropint collections su documents
db.films.drop()


x = films.insert_many(films_list) #documents priskirti collections
y = directors.insert_many(directors_list)

#for y in films.find():
# print(y)



#print(client.list_database_names()) #parodo visas db
#print(db.list_collection_names()) #parodo visus collections

# filmai su aukštesniu nei 5 reitingu
query1 = { "rating": { "$gt": 5 } }
mydoc1 = films.find(query1)
for x in mydoc1:
  print(x)

# directors, kurių vardas iš J
query2 = { "name": { "$regex": "^J" } }
mydoc2 = directors.find(query2)
for y in mydoc2:
  print(y)

# filmai, kuriuose vaidina žmogus arba žmonės, kurių pavardės prasideda iš B
query3 = { "actor.lastName": { "$regex": "^B" } }
mydoc3 = films.find(query3)
for x in mydoc3:
  print(x)

# rasti filmą Movie6
print(films.find_one( {}, { "title": "Movie6"}))

# rasti filmus pagal id
rez = films.find({
    "_id" : { "$in" : [ 2, 4, 7] }
})
for i in rez:
    print(i)


# agregacija filmų dokumentams suskaičiuoti
cursor = films.aggregate([{"$group":
                            {"_id": "$None",
                             "total films": {"$sum": 1}
                             }
                          }])
for document in cursor:
  print(document)


# agregavimas filmų kategorijų pasikartojimui suskaičiuoti
cursor2 = films.aggregate(
    [{
    "$group" :
        {"_id" : "$category",
         "count" : {"$sum" : 1}
         }}
    ])
for i in cursor2:
    print(i)

# agregacija filmų, kuriuose vaidina John
cursor3 = films.aggregate([
    {
        "$match" :
                 {"actor.firstName" : {"$eq" : "John" } }
    },
    {
        "$count" : "total_rows"
    }
])
for i in cursor3:
    print(i)



#mapreduce
    mapF = Code(" function() {"
                "   {"
                    "emit(this.title, this.rating);"
                    "    }"
                    "};")

    reduceF = Code(" function(title, rating){"
                   "    return Array.sum(rating);"
                   "};")

    result = db.command(
        'mapReduce',
        'films',
        map=mapF,
        reduce=reduceF,
        out={'inline': 1}
    )
    print("")
    print(result)
    print("")

# pymongo version

print(pymongo.version)


